package video2Day7;

public class UncheckedExceptions {
//RunTimeExceptions
	public UncheckedExceptions() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("========= Start of program ============\n");
		int a=12, b=0, answer=0;
		try {
			answer = a/b;
			System.out.println("a="+a+" b="+b+" answer = "+answer);
		} catch (ArithmeticException e) {
			// TODO Auto-generated catch block
			System.out.println("=========== An Arithmetic ERROR occurred ===========\n");
			e.printStackTrace();
		}
		System.out.println("============== The statement after the first block of checked ");
		double[] dValues=new double[3];
		dValues[0] = 44.7;
		dValues[1] = 99;
		dValues[2] = 12.6;
	//	dValues[3] = 100.936;
		for(double d:dValues)
		{
			System.out.println(" d= "+d);
		}
		System.out.println("========= End of program ============\n");
	}

}

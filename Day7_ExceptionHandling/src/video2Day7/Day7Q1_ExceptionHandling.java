package video2Day7;

import java.util.Scanner;

public class Day7Q1_ExceptionHandling {

	public Day7Q1_ExceptionHandling() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int age=0;
		do
		{
		try {
				Employee emp= new Employee("Jhon","Trovolta", 30, 2500.2);
				Scanner kbd = new Scanner(System.in);
				System.out.print("\n\nEnter Employee's"+emp.getFirstName()+" "+emp.getSurname()+" age: ");
				age=kbd.nextInt();
				System.out.println("\nBefore emp.setAge(age)= "+age+"\n");
				emp.setAge(age);
				System.out.println("\nAfter emp.setAge(age);, in case of exception, this will no be print");
			}
		 catch (EmployeeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
		while(age<22 || age>66);
		System.out.println("Employee Data:"); //To do since emp:Employer is in try/catch context it is not available here 
		System.out.println("\n ***************End of the Program, Have a nice working day*****************");
		
			
	} //Main end

}

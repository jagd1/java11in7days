package video2Day7;

public class Circle extends TwoDShape implements DrawShape, Comparable<Circle>  // we are only comparing Circle
{
	private double radius;
	public Circle()
	{
		super(1,1); // calls super class constructor TwoDShape
		radius =1;
	}
	public Circle(double newRadius) throws CircleException
	{
		super(1,1); // calls super class constructor, must be the first statement in the constructor
		if(newRadius <=0)
		{/*
			System.out.println("A circle must have a radius >0, Radius="+newRadius);
			System.out.println("The radius is invalid, the program is oging to exit");
			System.exit(0);
			*/
			throw new CircleException("Radius cannot be negative");
		}
		else	
		 radius =newRadius;
	}
	public Circle(int x, int y)
	{
		super(x,y); // calls super class constructor TwoDShape
		radius =1.0;
	}
	
	public Circle(double newRadius, int x, int y) throws CircleException //method overloading
	{
		super(x,y); // calls super class constructor TwoDShape
		if(newRadius <=0)
		{/*
			System.out.println("A circle must have a radius >0, Radius="+newRadius);
			System.out.println("The radius is invalid, the program is oging to exit");
			System.exit(0);
			*/
			throw new CircleException("Radius cannot be negative");
		}
		else	
		 radius =newRadius;
	}
	public void setRadius(double newRadius) throws CircleException
	{
		if(newRadius <=0)
		{/*
			System.out.println("A circle must have a radius >0, Radius="+newRadius);
			System.out.println("The radius is invalid, the program is oging to exit");
			System.exit(0);
			*/
			throw new CircleException("Radius cannot be negatige");
		}
		else	
		 radius =newRadius;
	}
	public double getRadius()
	{
		return radius;
	}
	public double calculateArea()
	{
		double area=0;
		//area = Math.PI*radius*radius;
		area = Math.PI;//Math.pow(radius, 2);
		return area;
	}
	public double calculatePerimeter()
	{
		double perimeter=0;
		perimeter= 2 * Math.PI*radius;
		return perimeter;
	}
	
	public String toString()
	{//Debug class, print the aguments
		return "radius = " + radius;
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		System.out.println("           **                ");
		System.out.println("        *      *             ");
		System.out.println("       *        *            ");
		System.out.println("     *            *          ");
		System.out.println("       *        *            ");
		System.out.println("        *      *             ");
		System.out.println("           **                ");
	}

	@Override
	public int compareTo(Circle inComingCircle) {
		// TODO Auto-generated method stub
		
		if(this.radius == inComingCircle.radius)
			return 0;
		if(this.radius > inComingCircle.radius)
			return 1;
		else
			return -1;
	}

}

package video2Day7;

import java.io.*;

public class CheckedExceptions {

	public CheckedExceptions() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File file;
		FileReader fr;
		BufferedReader br;
		String companyName =null;
		
		file =new File("resource/company.txt");
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			companyName = br.readLine();
			while(companyName != null)
			{
				System.out.println("Company name: "+companyName);
				companyName = br.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

package video2Day7;

public abstract class TwoDShape {
//  Top  level class for any 2D object
//  it defines the position of the shape
	private int xPos;
	private int yPos;
	public TwoDShape(int x, int y)
	{
		xPos = x;
		yPos = y;
	}
	public int getXPos()
	{
		return xPos;
	}
	public void setXpos(int x)
	{
		xPos=x;
	}
	public int getYPos()
	{
		return yPos;
	}
	public void setYpos(int y)
	{
		this.yPos=y;
	}
	
	public abstract double calculateArea();
	public abstract double calculatePerimeter();
	
	public String toString()
	{
		return "TwoDShape [xPos="+xPos+", yPos="+yPos+"]";
	}
}

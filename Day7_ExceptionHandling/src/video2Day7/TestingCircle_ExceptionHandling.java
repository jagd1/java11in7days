package video2Day7;

public class TestingCircle_ExceptionHandling {

	public TestingCircle_ExceptionHandling() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("===== Setting up the first Circle ==========");
		try {
			Circle c1 = new Circle(10);
			System.out.println(c1.toString());
		} catch (CircleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("=============== Setting up the second circle =========");
		try {
			Circle c2 = new Circle(-2);
			System.out.println(c2.toString());
		} catch (CircleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Recovered from errors you can go on ");
	}

}

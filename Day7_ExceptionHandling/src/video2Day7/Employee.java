package video2Day7;

public class Employee {
	private String firstName;
	private String surname;
	private int age;
	private double salary;
	public Employee(String name, String surname, int age, double salary) throws EmployeeException{
		// TODO Auto-generated constructor stub
	firstName=name;
	this.surname= surname; // you must use this refers to current class, to resolve name scope conflicts
	
	if(age >63 || age <22)
	{		
		throw new EmployeeException("The employee age must be <63 and >22");
	}
	this.age = age;
	this.salary=salary;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public String getSurname()
	{
		return surname;
	}
	public double getAge()
	{
		return age;
	}
	public double getSalary()
	{
		return salary;
	}
	public void setFirstName(String name)
	{
		firstName=name;
	}
	public void setSurname(String surname)
	{
		this.surname=surname;
	}
	public void setAge(int age) throws EmployeeException
	{
		if(age >63 || age <22)
		{		
			throw new EmployeeException("The employee age must be <63 and >22");
		}
		this.age=age;
	}
	public void setSalary(double salary)
	{
		this.salary=salary;
	}
	public String toString()
	{
		return "Name="+firstName+" Surname="+surname+" Age=" +age+" Salary="+salary;
	}
}

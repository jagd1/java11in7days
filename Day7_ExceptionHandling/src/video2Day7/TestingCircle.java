package video2Day7;

public class TestingCircle {

	public TestingCircle() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws CircleException {
		// TODO Auto-generated method stub
		double [] values = new double [] {22.4,1.9,7.778, 3.3};
		System.out.println("\n =========== Printing out the contents of the array  called values ==========");
		for(int pos=0; pos<values.length; pos++)
		{
			System.out.println("Values["+pos+"]="+values[pos]);
		}
		System.out.println("\n ============ Setting up the first circle ===========");
		Circle c1;
		try {
			c1= new Circle(10,15,15);
			System.out.println("c1 Radius = "+c1.getRadius());
			System.out.println("C1 area=" +c1.calculateArea());
			System.out.println("c1 perimeter="+c1.calculatePerimeter());
		} catch (CircleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("\n\n ===== Setting up the second Circle ======");
		Circle c2;
		try
		{
			c2 = new Circle(-1,0,0);
			System.out.println("C1 area=" +c2.calculateArea());
			System.out.println("c1 perimeter="+c2.calculatePerimeter());
		}
		catch(CircleException e)
		{
			e.printStackTrace();
		}
		
		System.out.println("Recovered from error an can make ammends ...");
		System.out.println("\n <=============== Re-creating c2 =========> ");
		try
		{
			c2 = new Circle(2,0,0);
			System.out.println("C1 area=" +c2.calculateArea());
			System.out.println("c1 perimeter="+c2.calculatePerimeter());
		}
		catch(CircleException e)
		{e.printStackTrace();}
		
	} //End main
	
	
}
